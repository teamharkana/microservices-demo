package serviceb.serviceb.controller;


import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import serviceb.serviceb.entity.Recommand;
import serviceb.serviceb.entity.RecommandResource;


@RestController
public class BasicController {
	
	@Autowired
	private RestTemplate restTemplate;
	
	
	@Autowired
	private LoadBalancerClient client;
	
	private ServiceInstance instance;
	
	@Autowired
	private Logger logger;
		

	
	
	@GetMapping("/home")
	public @ResponseBody String hello() {
		logger.info("ServiceB@Print Hello");
		return "Hello from B";
	}
	
	
	@GetMapping("/c")
	public @ResponseBody RecommandResource hello_c() {
		instance = serviceInstance();
		logger.info("ServiceB@Print Hello");
		logger.info("Service B@Call C@" + "http://" + this.instance.getHost() + ":" + this.instance.getPort());
		return this.restTemplate.getForObject("http://" + this.instance.getHost() + ":" + this.instance.getPort() + "/api", RecommandResource.class);
	}
	
	@GetMapping("/c/{id}")
	public @ResponseBody Recommand hello_c_id(@PathVariable Long id) {
		instance = serviceInstance();
		logger.info("ServiceB@Print Hello");
		logger.info("Service B@Call C@" + "http://" + this.instance.getHost() + ":" + this.instance.getPort());
		return this.restTemplate.getForObject("http://" + this.instance.getHost() + ":" + this.instance.getPort() + "/api/" + id, Recommand.class);
	}
	
	public ServiceInstance serviceInstance() {
		return client.choose("service-c");
	}
}
