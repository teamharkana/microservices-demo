package serviceb.serviceb;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.web.client.RestTemplate;

import serviceb.serviceb.entity.Review;

@SpringBootApplication
//@EnableDiscoveryClient
public class ServiceBApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceBApplication.class, args);
	}
	
	@Autowired
	private RepositoryRestConfiguration configuration;
	
	@PostConstruct
	public void init() {
		this.configuration.exposeIdsFor(Review.class);
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public Logger logger() {
		return LoggerFactory.getLogger(ServiceBApplication.class);
	}
}
