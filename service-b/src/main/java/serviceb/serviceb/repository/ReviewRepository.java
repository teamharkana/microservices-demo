package serviceb.serviceb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import serviceb.serviceb.entity.Review;

@RestResource(path = "api")
public interface ReviewRepository extends JpaRepository<Review, Long>{

}
