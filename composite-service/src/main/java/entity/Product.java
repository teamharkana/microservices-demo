/**
 * 
 */
package entity;

import java.sql.Timestamp;

/**
 * @author thomas.simoes
 *
 */

public class Product extends BaseSerializable {
	private static final long serialVersionUID = 1674203056506348309L;

	private Long id;
	private String name;
	private String description;
	private String photo;
	private Timestamp date_post;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Timestamp getDate_post() {
		return date_post;
	}

	public void setDate_post(Timestamp date_post) {
		this.date_post = date_post;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	public static Product fromProductFinal(ProductFinal productFinal) {
		Product p = new Product();
		
		p.setName(productFinal.getName());
		p.setDescription(productFinal.getDescription());
		p.setDate_post(productFinal.getDate_post());
		p.setPhoto(productFinal.getPhoto());
		p.setId(productFinal.getId());
		return p;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", photo=" + photo
				+ ", date_post=" + date_post + "]";
	}
}
