package entity;

public class Review extends BaseSerializable  {
	private static final long serialVersionUID = 5225282779746276401L;
	private Long	productId;
	private int 	count;
	

	public static Review fromProductFinal(ProductFinal productFinal) {
		Review review = new Review();
		
		review.setCount(productFinal.getReview());
		review.setProductId(productFinal.getId());
		return review;
	}
	@Override
	public String toString() {
		return "Review [product_id=" + productId + ", count=" + count + "]";
	}

	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
}
