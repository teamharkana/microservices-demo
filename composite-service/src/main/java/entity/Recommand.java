package entity;

public class Recommand extends BaseSerializable  {
	private static final long serialVersionUID = -6132359784775182549L;
	private Long productId;
	private int count;
	
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public static Recommand fromProductFinal(ProductFinal productFinal) {
		Recommand r = new Recommand();
		
		r.setCount(productFinal.getRecommand());
		r.setProductId(productFinal.getId());
		return r;
	}
	@Override
	public String toString() {
		return "Recommand [productId=" + productId + ", count=" + count + "]";
	}
}
