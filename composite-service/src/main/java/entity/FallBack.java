package entity;

public class FallBack extends BaseSerializable {
	private static final long serialVersionUID = 5402457715007752208L;
	private String message;
	
	public FallBack(String mes) {
		this.message = mes;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
