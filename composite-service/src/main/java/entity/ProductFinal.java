package entity;

import java.sql.Timestamp;

public class ProductFinal extends BaseSerializable  {
	private static final long serialVersionUID = -7223810553193934377L;
	private Long id;
	private String name;
	private String description;
	private String photo;
	private Timestamp date_post;
	private int review;
	private int recommand;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public Timestamp getDate_post() {
		return date_post;
	}
	public void setDate_post(Timestamp date_post) {
		this.date_post = date_post;
	}
	public int getReview() {
		return review;
	}
	public void setReview(int review) {
		this.review = review;
	}
	public int getRecommand() {
		return recommand;
	}
	public void setRecommand(int recommand) {
		this.recommand = recommand;
	}
	public void addProduct(Product product) {
		setName(product.getName());
		setDescription(product.getDescription());
		setDate_post(product.getDate_post());
		setPhoto(product.getPhoto());
		setId(product.getId());
	}
	public void addReview(Review review) {
		setReview(review.getCount());
	}
	public void addRecommand(Recommand recommand) {
		setRecommand(recommand.getCount());
	}
}
