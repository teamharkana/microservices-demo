package entity;

import java.util.List;

public class ProductResource extends BaseSerializable  {
	private static final long serialVersionUID = -6958830773977831469L;
	private EmbeddedProduct _embedded;
	
	public EmbeddedProduct get_embedded() {
		return _embedded;
	}

	public void set_embedded(EmbeddedProduct _embedded) {
		this._embedded = _embedded;
	}

	public class EmbeddedProduct {
		private List<Product> products;

		public List<Product> getProducts() {
			return products;
		}

		public void setProducts(List<Product> products) {
			this.products = products;
		}
	}
}
