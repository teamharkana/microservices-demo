package entity;

import java.util.List;

public class RecommandResource extends BaseSerializable  {
	private static final long serialVersionUID = -1354281167960292621L;
	private EmbeddedRecommand _embedded;
		
	public EmbeddedRecommand get_embedded() {
		return _embedded;
	}

	public void set_embedded(EmbeddedRecommand _embedded) {
		this._embedded = _embedded;
	}

	public class EmbeddedRecommand {
		private List<Recommand> recommands;

		public List<Recommand> getRecommands() {
			return recommands;
		}

		public void setRecommands(List<Recommand> recommands) {
			this.recommands = recommands;
		}
	}
}

