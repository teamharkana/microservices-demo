package compositeservice.compositeservice.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import entity.BaseSerializable;
import entity.FallBack;
import entity.Product;
import entity.ProductFinal;
import entity.ProductResource;
import entity.Recommand;
import entity.RecommandResource;
import entity.Review;
import entity.ReviewResource;

@RestController
public class CompositeController {
	@Autowired
	private LoadBalancerClient ribbon;	
	@Autowired
	private RestTemplate restTemplate;
	
	public static String AND = "and";
	public static String OR = "or";
	
	@GetMapping("/home")
	public String composite() {
		return "Welcome on composite service";
	}
		
	@HystrixCommand(fallbackMethod = "notAvailable")
	@GetMapping("/{service}")
	public BaseSerializable getService(@PathVariable String service) {
		ServiceInstance instance = ribbon.choose("service-" + service);
		switch (service)
		{
			case "a":
				return restTemplate.getForObject(instance(instance) + "/api", ProductResource.class);
			case "b":
				return restTemplate.getForObject(instance(instance) + "/api", ReviewResource.class);
			case "c":
				return restTemplate.getForObject(instance(instance) + "/api", RecommandResource.class);
		}
		return null;
	}
	
	@GetMapping("/{service}/{id}")
	@HystrixCommand(fallbackMethod = "notAvailableId")
	public BaseSerializable getServiceParam(@PathVariable String service, @PathVariable Long id) {
		ServiceInstance instance = ribbon.choose("service-" + service);
		switch (service)
		{
			case "a":
				return restTemplate.getForObject(instance(instance) + "/api/" + id, Product.class);
			case "b":
				return restTemplate.getForObject(instance(instance) + "/api/" + id, Review.class);
			case "c":
				return restTemplate.getForObject(instance(instance) + "/api/" + id, Recommand.class);
		}
		return null;
	}
	
	/*
	@GetMapping("/{s1}/{operator}/{s2}")
	public String s1Ands2(@PathVariable String s1, @PathVariable String operator, @PathVariable String s2) {
		ServiceInstance s1Instance = ribbon.choose("service-" + s1);
		ServiceInstance s2Instance = ribbon.choose("service-" + s2);
		String content = "";
		
		if (s1Instance == null && operator.equals(AND)) {
			return "Premier service non trouvé";
		}
		if (s2Instance == null && operator.equals(AND)) {
			return "Deuxième service non trouvé";
		}
		if (s1Instance == null && s2Instance == null) {
			return "Aucun service trouvé";
		}
		if (s1Instance != null) {
			content = restTemplate.getForObject(instance(s1Instance), String.class) + "\n";			
		}
		if (operator.equals(AND) || (s1Instance == null && operator.equals(OR))) {
			content += restTemplate.getForObject(instance(s2Instance), String.class);			
		}
		if (s1.equals("a")) {
			final ProductResource pr = restTemplate.getForObject(instance(s1Instance) + "/api", ProductResource.class);
			
		}
		return null;
	}
	*/
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		try {
			restTemplate.delete(instance(ribbon.choose("service-a")) + "/api/" + id);	
		} catch (HttpClientErrorException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		try {
			restTemplate.delete(instance(ribbon.choose("service-b")) + "/api/" + id);
			restTemplate.delete(instance(ribbon.choose("service-c")) + "/api/" + id);			
		}catch (HttpClientErrorException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping("/details/{id}")
	@HystrixCommand(fallbackMethod = "notAvailableLong")
	public ResponseEntity<BaseSerializable> get(@PathVariable Long id) {
		ProductFinal pf = new ProductFinal();
		ServiceInstance instanceA = ribbon.choose("service-a");
		ServiceInstance instanceB = ribbon.choose("service-b");
		ServiceInstance instanceC = ribbon.choose("service-c");
		
		try {
			pf.addProduct(restTemplate.getForObject(instance(instanceA) + "/api/" + id, Product.class));			
			pf.addRecommand(restTemplate.getForObject(instance(instanceB) + "/api/" + id, Recommand.class));			
			pf.addReview(restTemplate.getForObject(instance(instanceC) + "/api/" + id, Review.class));
		}catch (HttpClientErrorException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(pf, HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<Long> save(@RequestBody ProductFinal productFinal) {
		Product product = Product.fromProductFinal(productFinal);
		try {
			product.setId(restTemplate.postForObject(instance(ribbon.choose("service-a")) + "/api", product, Product.class).getId());			
		} catch (HttpClientErrorException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}

		productFinal.setId(product.getId());
		Recommand recommand = Recommand.fromProductFinal(productFinal);
		Review	review = Review.fromProductFinal(productFinal);
		try {
			restTemplate.postForObject(instance(ribbon.choose("service-b")) + "/api", review, Review.class);
			restTemplate.postForObject(instance(ribbon.choose("service-c")) + "/api", recommand, Recommand.class);			
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(product.getId(), HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Void> update(@RequestBody ProductFinal productFinal, @PathVariable Long id) {
		Product product = Product.fromProductFinal(productFinal);
		if (get(id).getStatusCodeValue() == 404) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		try {
			restTemplate.put(instance(ribbon.choose("service-a")) + "/api/" + id, product, Product.class);			
			productFinal.setId(product.getId());
		} catch (HttpClientErrorException e) {
			return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
		}
		Recommand recommand = Recommand.fromProductFinal(productFinal);
		Review	review = Review.fromProductFinal(productFinal);
		try {
			restTemplate.put(instance(ribbon.choose("service-b")) + "/api/" + id, review, Review.class);
			restTemplate.put(instance(ribbon.choose("service-c")) + "/api/" + id, recommand, Recommand.class);			
		}catch (HttpClientErrorException e) {
			return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping("/")
	public List<ProductFinal> getAll() {
		ServiceInstance instance = ribbon.choose("service-a");
		final ProductResource pr =  restTemplate.getForObject(instance(instance) + "/api", ProductResource.class);

		if (pr.get_embedded().getProducts() == null) {
			return null;
		}
		List<ProductFinal> list = pr.get_embedded().getProducts().stream().map(product -> {
			final ProductFinal pf = new ProductFinal();
			
			pf.addProduct(product);
			return pf;
		}).collect(Collectors.toList());
		return list;
	}
	

	public BaseSerializable notAvailable(String param) {
		return new FallBack("Erreur " + param);
	}
	
	public ResponseEntity<BaseSerializable> notAvailableLong(Long param) {
		return new ResponseEntity<>(new FallBack("Erreur " + param), HttpStatus.OK);
	}

	public BaseSerializable notAvailableId(String param, Long id) {
		return new FallBack("Id : " + id + " Erreur : " + param);
	}
	
	public String instance(ServiceInstance instance) {
		return "http://" + instance.getHost() + ":" + instance.getPort();
	}
}
