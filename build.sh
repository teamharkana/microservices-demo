#! /bin/bash

a=("service-d" "service-a" "service-b" "service-c" "service-e" "service-f" "service-g" "service-h" "composite-service")
b=${#a}
c=0

while [ $c -lt $b ]
do
  d=${a[$c]}
  cd $d && mvn clean install spring-boot:run && cd ..
  c=$(( $c + 1 ))
done
