/**
 * 
 */
package servicea.servicea.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author thomas.simoes
 *
 */
@Entity
public class Product implements Serializable {
	private static final long serialVersionUID = 8507642888830858061L;

	@Id
	@GeneratedValue
	private Long id;
	@Column(unique = true)
	private String name;
	private String description;
	private String photo;
	private Timestamp date_post;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Timestamp getDate_post() {
		return date_post;
	}

	public void setDate_post(Timestamp date_post) {
		this.date_post = date_post;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", photo=" + photo
				+ ", date_post=" + date_post + "]";
	}
}
