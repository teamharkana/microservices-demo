package servicea.servicea.entity;

import java.util.List;

public class RecommandResource {
	private EmbeddedRecommand _embedded;
		
	public EmbeddedRecommand get_embedded() {
		return _embedded;
	}

	public void set_embedded(EmbeddedRecommand _embedded) {
		this._embedded = _embedded;
	}

	public class EmbeddedRecommand {
		private List<Recommand> recommands;

		public List<Recommand> getRecommands() {
			return recommands;
		}

		public void setRecommands(List<Recommand> recommands) {
			this.recommands = recommands;
		}
	}
}

