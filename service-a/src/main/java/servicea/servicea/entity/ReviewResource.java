package servicea.servicea.entity;

import java.util.List;

public class ReviewResource {
	private EmbeddedReview _embedded;
	
	public EmbeddedReview get_embedded() {
		return _embedded;
	}

	public void set_embedded(EmbeddedReview _embedded) {
		this._embedded = _embedded;
	}

	public class EmbeddedReview {
		private List<Review> reviews;

		public List<Review> getReviews() {
			return reviews;
		}

		public void setReviews(List<Review> reviews) {
			this.reviews = reviews;
		}
	}
}
