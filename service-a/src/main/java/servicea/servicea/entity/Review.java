package servicea.servicea.entity;

public class Review {
	private Long	productId;
	private int 	count;
	
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "Review [productId=" + productId + ", count=" + count + "]";
	}
}
