package servicea.servicea.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import servicea.servicea.entity.Product;

@RepositoryRestResource(path = "api")
public interface ProductRepository extends JpaRepository<Product, Long> {

}
