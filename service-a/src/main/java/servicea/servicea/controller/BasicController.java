package servicea.servicea.controller;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import servicea.servicea.entity.Recommand;
import servicea.servicea.entity.RecommandResource;
import servicea.servicea.entity.Review;
import servicea.servicea.entity.ReviewResource;

@RestController
public class BasicController {
	
	private ServiceInstance instance;
	
	@Autowired
	private LoadBalancerClient client;

	
	@Autowired
	private RestTemplate restTemplate;
		
		
	@Autowired
	private Logger logger;	

	
	@GetMapping("/home")
	public @ResponseBody String hello() {
		logger.info("Service A@Call A");
		return "Hello from A";
	}
	
	@GetMapping("/b")
	public @ResponseBody ReviewResource hello_b() {
		instance = serviceInstance();
		
		logger.info("Service A@Call B@" + "http://" + this.instance.getHost() + ":" + this.instance.getPort());
		return this.restTemplate.getForObject("http://" + this.instance.getHost() + ":" + this.instance.getPort() + "/api", ReviewResource.class);
	}
	
	@GetMapping("/b/{id}")
	public @ResponseBody Review hello_b_id(@PathVariable Long id) {
		instance = serviceInstance();
		
		logger.info("Service A@Call B@" + "http://" + this.instance.getHost() + ":" + this.instance.getPort());
		return this.restTemplate.getForObject("http://" + this.instance.getHost() + ":" + this.instance.getPort() + "/api/" + id, Review.class);
	}
	
	@GetMapping("/c")
	public @ResponseBody RecommandResource hello_c() {
		instance = serviceInstance();
		
		logger.info("Service A@Call B@" + "http://" + this.instance.getHost() + ":" + this.instance.getPort());
		return this.restTemplate.getForObject("http://" + this.instance.getHost() + ":" + this.instance.getPort() + "/c", RecommandResource.class);
	}
	
	@GetMapping("/c/{id}")
	public @ResponseBody Recommand hello_c_id(@PathVariable Long id) {
		instance = serviceInstance();
		
		logger.info("Service A@Call B@" + "http://" + this.instance.getHost() + ":" + this.instance.getPort());
		return this.restTemplate.getForObject("http://" + this.instance.getHost() + ":" + this.instance.getPort() + "/c/" + id, Recommand.class);
	}
	
	public ServiceInstance serviceInstance() {
		return this.client.choose("service-b");
	}
}
