package servicee.servicee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableConfigServer
@EnableDiscoveryClient
public class ServiceEApplication {

	@GetMapping("/")
	public String hello() {
		return "Welcome on config server";
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ServiceEApplication.class, args);
	}
}
