package servicec.servicec.controller;



import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BasicController {
	
	@Autowired
	private Logger logger;
		
	@GetMapping("/home")
	public @ResponseBody String hello() {
		logger.info("ServiceC@Print Hello");
		return "Hello from C";
	}
}
