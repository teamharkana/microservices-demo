package servicec.servicec;



import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

import servicec.servicec.entity.Recommand;

@SpringBootApplication
@EnableDiscoveryClient
public class ServiceCApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceCApplication.class, args);
	}
	
	@Autowired
	private RepositoryRestConfiguration configuration;
	
	@PostConstruct
	public void init() {
		this.configuration.exposeIdsFor(Recommand.class);
	}
	
	@Bean
	public Logger logger() {
		return LoggerFactory.getLogger(ServiceCApplication.class);
	}
}
