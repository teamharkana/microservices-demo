package servicec.servicec.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Recommand {
	@Id
	@Column(unique = true, updatable= false)
	private Long productId;
	private int count;
	
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "Recommand [productId=" + productId + ", count=" + count + "]";
	}
}
