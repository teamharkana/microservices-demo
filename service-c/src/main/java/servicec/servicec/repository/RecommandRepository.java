package servicec.servicec.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import servicec.servicec.entity.Recommand;

@RestResource(path = "api")
public interface RecommandRepository extends JpaRepository<Recommand, Long>{

}
